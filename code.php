<?php

class Person{
   public function __construct($firstName, $middleName, $lastName){
      $this->firstName = $firstName;
      $this->middleName = $middleName;
      $this->lastName = $lastName;
   }
   function printName(){
      return "Your full name is $this->firstName $this->middleName $this->lastName.";
   }
}

class Developer extends Person{
   function printName(){
      return "Your name is $this->firstName $this->middleName $this->lastName and you are a developer";
   }
}

class Engineer extends Person{
   function printName(){
      return "You are an engineer named $this->firstName $this->middleName $this->lastName.";
   }
}